FROM php:8.2-fpm-alpine3.18

ENV PORT=8000

RUN apk add --no-cache \
    supervisor \
    nginx \
    unzip \
    libxml2-dev \
    oniguruma-dev \
    curl-dev \
    zip \
    git

RUN docker-php-ext-install curl dom mbstring pdo_mysql opcache

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

COPY ./container/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY ./container/php/php.ini /usr/local/etc/php/php.ini
COPY ./container/php/php-fpm.conf /usr/local/etc/php-fpm.d/www.conf

COPY ./container/nginx/nginx.conf /etc/nginx/nginx.conf

WORKDIR /var/www

# Copy code
COPY --chown=www-data . .

EXPOSE ${PORT}

ENTRYPOINT [ "container/entrypoint.sh" ]
