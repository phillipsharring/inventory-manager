# Inventory Manager

![Inventory Manager](./icon.png)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=phillipsharring_inventory-manager&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=phillipsharring_inventory-manager)

To run this,

1. `cp .env.example .env`
2. Modify the values in `.env` to suit.
3. `docker compose build`
4. `docker compose up -d`
5. Visit http://localhost:8000.

The container will run `composer install` when it starts, so no need to do that.

6. Visit http://localhost:8080 to view the Adminer interface. Login with the values you selected in the .env file.

Thanks! 🙂

#### Notes

1. There is almost _zero_ backend validation.
2. 4 files were requested, but "follow the best OOP practices for backend" was also requested, hence the `src` directory & the namespacing. The PSR-4 mapping is done in the `composer.json` file.
