#!/bin/sh

cd /var/www

echo "Starting composer install"
composer install --no-ansi --no-dev --no-interaction --no-plugins --no-progress --quiet --optimize-autoloader
echo "Composer install finished"

chown -R www-data:www-data /run /var/lib/nginx /var/log/nginx

/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
