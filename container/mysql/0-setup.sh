#!/bin/sh

# Define directories
DOCKER_ENTRYPOINT_DIR="/docker-entrypoint-initdb.d"
TMP_SQL_DIR="/tmp/sql_temp"

# Ensure the temporary directory exists
mkdir -p "$TMP_SQL_DIR"

# Read environment variables
DATABASE_SCHEMA="${DATABASE_SCHEMA:-inventory}"
DATABASE_PREFIX="${DATABASE_PREFIX:-}"
DATABASE_USER="${DATABASE_USER:-manager}"
DATABASE_PASSWORD="${DATABASE_PASSWORD:-password}"

# Replace environment variables in the setup SQL script
cp /tmp/1-permissions.sql "$TMP_SQL_DIR"
cp /tmp/2-schema.sql "$TMP_SQL_DIR"
cp /tmp/3-data.sql "$TMP_SQL_DIR"

# Replace environment variables in the setup SQL script
sed "s/\${DATABASE_SCHEMA}/${DATABASE_SCHEMA}/g" "$TMP_SQL_DIR/1-permissions.sql" > "$TMP_SQL_DIR/1-permissions.sql.tmp"
sed "s/\${DATABASE_USER}/${DATABASE_USER}/g" "$TMP_SQL_DIR/1-permissions.sql.tmp" > "$TMP_SQL_DIR/1-permissions.sql.tmp2"
sed "s/\${DATABASE_PASSWORD}/${DATABASE_PASSWORD}/g" "$TMP_SQL_DIR/1-permissions.sql.tmp2" > "$TMP_SQL_DIR/1-permissions.sql.tmp3"

# Replace environment variables in the schema SQL script
sed "s/\${DATABASE_SCHEMA}/${DATABASE_SCHEMA}/g" "$TMP_SQL_DIR/2-schema.sql" > "$TMP_SQL_DIR/2-schema.sql.tmp"
sed "s/\${DATABASE_PREFIX}/${DATABASE_PREFIX}/g" "$TMP_SQL_DIR/2-schema.sql.tmp" > "$TMP_SQL_DIR/2-schema.sql.tmp2"

# Replace environment variables in the data SQL script
sed "s/\${DATABASE_SCHEMA}/${DATABASE_SCHEMA}/g" "$TMP_SQL_DIR/3-data.sql" > "$TMP_SQL_DIR/3-data.sql.tmp"
sed "s/\${DATABASE_PREFIX}/${DATABASE_PREFIX}/g" "$TMP_SQL_DIR/3-data.sql.tmp" > "$TMP_SQL_DIR/3-data.sql.tmp2"

cat "$TMP_SQL_DIR/1-permissions.sql.tmp3" >> "$DOCKER_ENTRYPOINT_DIR/initdb.sql"
cat "$TMP_SQL_DIR/2-schema.sql.tmp2" >> "$DOCKER_ENTRYPOINT_DIR/initdb.sql"
cat "$TMP_SQL_DIR/3-data.sql.tmp2" >> "$DOCKER_ENTRYPOINT_DIR/initdb.sql"

# Clean up temporary directory
rm -rf "$TMP_SQL_DIR"
