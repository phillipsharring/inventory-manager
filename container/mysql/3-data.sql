USE ${DATABASE_SCHEMA};

INSERT INTO
    ${DATABASE_PREFIX}items (id, item, item_type)
VALUES
    (11, 'A4 Sheet', 1),
    (12, 'Notebook', 1),
    (5, 'Clear Tape', 1),
    (3, 'Marker', 1),
    (10, 'Paper Clip', 1),
    (1, 'Pen', 1),
    (8, 'Thumbtack', 1),
    (2, 'Printer', 2),
    (4, 'Scanner', 2),
    (7, 'Shredder', 2),
    (6, 'Standing Table', 2),
    (13, 'Chair', 3),
    (14, 'Stool', 3);

INSERT INTO
    ${DATABASE_PREFIX}requests (requested_by, requested_on, items)
VALUES
    ('maya', '2023-04-01', '1,5,3'),
    ('kie', '2023-04-03', '2'),
    ('ron', '2023-04-10', '3,10'),
    ('maya', '2023-04-20', '4'),
    ('john', '2023-05-01', '5,12'),
    ('smith', '2023-05-04', '6'),
    ('john', '2023-05-10', '7'),
    ('lily', '2023-05-11', '8,11'),
    ('lily', '2023-05-12', '7'),
    ('lily', '2023-05-13', '13'),
    ('maya', '2023-05-14', '2'),
    ('lily', '2023-05-15', '14');

INSERT INTO
    ${DATABASE_PREFIX}summary (requested_by, items)
VALUES
    ('maya', '[{1,[1,5,3]}, {2,[4,2]}]'),
    ('kie', '[{2,[2]}]'),
    ('ron', '[{1, [3,10]}]'),
    ('john', '[{1,[5,12]}, {2,[7]}]'),
    ('smith', '[{2, [6]}]'),
    ('lily', '[{1,[8,11]},{2,[7]},{3,[13, 14]}]');
