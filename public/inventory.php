<?php

declare(strict_types=1);

namespace Inventory;

require_once __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use Inventory\Controllers\GetRequests;
use Inventory\Controllers\GetRequest;
use Inventory\Controllers\GetItems;
use Inventory\Controllers\NotFound;
use Inventory\Controllers\PostRequest;
use Inventory\Controllers\PutRequest;

$dotenv = Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

function env(string $key, string $default = null): string
{
    return $_ENV[$key] ?? $default;
}

$method = strtolower($_SERVER['REQUEST_METHOD']);
$action = $_GET['action'] ?? null;
$match = sprintf('%s_%s', $method, $action);

$controller = match ($match) {
    'get_requests' => new GetRequests(),
    'get_request' => new GetRequest(),
    'get_items' => new GetItems(),
    'post_request' => new PostRequest(),
    'put_request' => new PutRequest(),
    default => new NotFound(),
};

$status = $controller->getStatus();
$output = $controller();

http_response_code($status);
header('content-type: application/json');

echo $output;
