$(document).ready(function () {

    // A pencil square icon
    const pencilSquare = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
  <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
</svg>`;

    // A util for making API urls
    const url = (endpoint) => {
        const base = '/inventory.php?action='
        return `${base}${endpoint}`;
    };

    // The datatable
    const datatable = $('#requests').DataTable({
        ajax: {url: url('requests'), dataSrc: '' },
        columns: [
            { data: 'req_id' },
            { data: 'requested_by' },
            { data: 'items' },
            { data: 'request_type' },
            {
                data: null,
                className: 'text-center',
                // The edit button, which uses the pencil icon. The `edit-request` class is used to attach the click event
                render: function(data, type, full, _meta){
                    return `<button class="btn btn-primary btn-sn edit-request" data-req-id="${data.req_id}">${pencilSquare}</button>`;
                },
            },
        ],

        // Use the draw callback to attach the edit events.
        fnDrawCallback: function() {
            // Use off/on to prevent multiple events being attached.
            // Use event.namespace so it's possible to off/on more precisely.
            $('.edit-request').off('click.edit-request').on('click.edit-request', (e) => {
                e.preventDefault();
                // Use currentTarget so we always get the button element, even if the svg is clicked
                const reqId = $(e.currentTarget).data('req-id');
                // Call the API to get a request's data and populate the modal.
                $.get(url(`request`), { req_id: reqId }, (data) => {
                    openEditModal(data);
                });
            })
        }
    });

    // Get the items from API to populate the item selects
    let items;
    $.get(url('items'), (data) => {
        items = data;
    })

    // Get a reference to the modal
    const modal = new bootstrap.Modal('#request-modal');

    // Add an item select
    const addItemSelect = (itemId = 0, itemType = null) => {
        // Get the existing selects to figure out the id number
        const itemsCount = $('.item-row').length;
        const id = `item${itemsCount + 1}`;

        // Use document.createElement because it's more performant than the jQuery way
        // Make the row
        const row = document.createElement('div');
        row.classList.add('mb-3', 'row', 'item-row');

        // Make the label
        const label = document.createElement('label');
        label.setAttribute('for', id);
        label.classList.add('col-sm-2', 'col-form-label');
        label.innerText = 'Item';

        // Make the column
        const col = document.createElement('div');
        col.classList.add('col-sm-10');

        // Make the select
        const select = document.createElement('select');
        select.classList.add('form-select', 'item-select');
        select.setAttribute('id', id);
        select.setAttribute('arial-label', 'Item');

        // Give the select a 1st empty option
        const emptyOption = document.createElement('option');
        select.append(emptyOption);

        // Attach the items to the select
        // First filtering the items based on the item type.
        //     If this is the first select, all items will be added
        //     If another select has been added, only the items matching the previous selected item's type will display
        items.filter((item) => {
            return itemType === null || itemType === item.item_type
        })
            // Now that the items are filtered, add them as options to the select
            .forEach((item) => {
                const option = document.createElement('option');
                option.setAttribute('value', item.id);
                // Select the item if we are editing
                if (item.id === itemId) {
                    option.setAttribute('selected', 'selected');
                }
                option.innerText = item.item;
                select.append(option);
            });

        // Add everything back up the chain
        col.append(select);
        row.append(label);
        row.append(col);

        // Put this select into #item-selects
        $('#item-selects').append(row);
    }

    // Get an item type from a given item
    const getItemTypeFromId = (itemId) => {
        // filter the items to get the selected item
        const filtered = items.filter((item) => {
            return item.id === itemId;
        });
        const item = filtered[0];
        // return the item's type
        return item.item_type;
    }

    // Adds another select box for an additional item
    const addAnotherItem = () => {
        const itemsCount = $('.item-row').length;
        const itemId = parseInt($(`#item${itemsCount}`).val());
        // If there's no previously selected item, don't add this one.
        if (!itemId) {
            return;
        }
        // Get the previously selected item's type
        const itemType = getItemTypeFromId(itemId);
        // Add the select
        addItemSelect(0, itemType);
    }

    // A generic function for when we're done w/ adding or editing
    const saveRequestSuccess = function() {
        datatable.ajax.reload();
        modal.hide();
    };

    // Sends a new request to the API
    const addRequest = (e) => {
        e.preventDefault();
        const requested_by = $('#user').val();
        const items = [];
        $('.item-select').each(function () {
            items.push(parseInt($(this).val()));
        });
        const data = { requested_by, items }
        $.post(url('request'), data, saveRequestSuccess);
    };

    // Edits a request on the API
    const editRequest = (e) => {
        e.preventDefault();
        const req_id = $('#req_id').val();
        const requested_by = $('#user').val();
        const items = [];
        $('.item-select').each(function () {
            items.push(parseInt($(this).val()));
        });
        const data = { req_id, requested_by, items }

        // PUT because we're replacing the entire record
        $.ajax({
            type: 'PUT',
            url: url('request'),
            data,
            success: saveRequestSuccess
        });
    }

    // Open the modal when Adding a request
    const openAddModal = () => {
        modal.show();
        // Make sure we can edit the username
        $('#user').val('').removeAttr('disabled');
        // Clear any existing item selects
        $('#item-selects').empty();
        // Add one item select
        addItemSelect();
        // Attach the event to the "add more" link
        $('#add-more').off('click.add-more').on('click.add-more', () => {
            addAnotherItem();
        });
        // Attach the add request to the save button
        $('#save-request').off('click.save-request').on('click.save-request', addRequest)
    }

    // Open the modal when Editing a request
    const openEditModal = (data) => {
        modal.show();
        // Set the request ID
        $('#req_id').val(data.req_id);
        // Make sure we CANNOT edit the username
        $('#user').val(data.requested_by).attr('disabled', 'disabled');
        // Clear any existing item selects
        $('#item-selects').empty();
        // Add one item select for each item in the request
        data.items.forEach((item) => {
            addItemSelect(parseInt(item.id), parseInt(item.item_type));
        })
        // Attach the event to the "add more" link and the edit request to the save button
        $('#add-more').off('click.add-more').on('click.add-more', () => {
            addAnotherItem();
        });
        $('#save-request').off('click.save-request').on('click.save-request', editRequest)
    }

    // Attach the open modal event to the add request button
    $('#add-request').off('click.add-request').on('click.add-request', () => {
        openAddModal();
    })
});
