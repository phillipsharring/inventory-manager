<?php

declare(strict_types=1);

namespace Inventory\Controllers;

/**
 * A basic controller parent
 */
class Controller
{
    public const STATUS = 200;

    public function getStatus(): int
    {
        return static::STATUS;
    }
}
