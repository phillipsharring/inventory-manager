<?php

declare(strict_types=1);

namespace Inventory\Controllers;

use Inventory\Items\Items;
use JsonException;

class GetItems extends Controller
{
    /**
     * @throws JsonException
     */
    public function __invoke(): string
    {
        $items = new Items();
        return json_encode($items->all(), JSON_THROW_ON_ERROR);
    }
}
