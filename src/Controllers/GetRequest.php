<?php

declare(strict_types=1);

namespace Inventory\Controllers;

use Inventory\Requests\Requests;
use JsonException;

class GetRequest extends Controller
{
    /**
     * @throws JsonException
     */
    public function __invoke(): bool|string
    {
        $reqId = $_GET['req_id'];
        $requests = new Requests;
        return json_encode($requests->get($reqId), JSON_THROW_ON_ERROR);
    }
}
