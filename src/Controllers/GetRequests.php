<?php

declare(strict_types=1);

namespace Inventory\Controllers;

use Inventory\Requests\Requests;
use JsonException;

class GetRequests extends Controller
{
    /**
     * @throws JsonException
     */
    public function __invoke(): bool|string
    {
        $requests = new Requests;
        return json_encode($requests->all(), JSON_THROW_ON_ERROR);
    }
}
