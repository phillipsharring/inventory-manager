<?php

declare(strict_types=1);

namespace Inventory\Controllers;

/**
 * The fallback controller
 */
class NotFound extends Controller
{
    public const STATUS = 404;

    public function __invoke(): string
    {
        return 'not found';
    }
}
