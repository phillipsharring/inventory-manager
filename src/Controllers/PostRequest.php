<?php

declare(strict_types=1);

namespace Inventory\Controllers;

use Inventory\Requests\Requests;
use JsonException;

class PostRequest extends Controller
{
    public const STATUS = 201;

    /**
     * @throws JsonException
     */
    public function __invoke(): bool|string
    {
        $requests = new Requests;
        $reqId = $requests->add($_POST);
        $requests->makeSummary($_POST['requested_by']);
        return json_encode($requests->get($reqId), JSON_THROW_ON_ERROR);
    }
}
