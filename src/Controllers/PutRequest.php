<?php

declare(strict_types=1);

namespace Inventory\Controllers;

use Inventory\Requests\Requests;
use JsonException;

class PutRequest extends Controller
{
    public const STATUS = 202;

    /**
     * @throws JsonException
     */
    public function __invoke(): bool|string
    {
        // PHP doesn't have a $_PUT scope. Could have used a POST and done something else, but went this direction
        // Very unsafe
        parse_str(file_get_contents('php://input'), $_PUT);
        $requests = new Requests;
        $reqId = $requests->edit($_PUT);
        $requests->makeSummary($_PUT['requested_by']);
        return json_encode($requests->get($reqId), JSON_THROW_ON_ERROR);
    }
}
