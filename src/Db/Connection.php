<?php

declare(strict_types=1);

namespace Inventory\Db;

use Inventory;
use PDO;

/**
 * A basic PDO wrapper w/ just enough functionality
 */
class Connection
{
    // Normally we would use dotenv and not hardcode these values
    private string $host = '';

    private string $db   = '';

    private string $user = '';

    private string $pass = '';

    private string $charset = 'utf8mb4';

    private array $options = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];

    private PDO $connection;

    // A singleton instance
    private static null|Connection $instance = null;

    private function __construct() {
        // Get values from .env file
        $this->host = Inventory\env('DB_HOST');
        $this->db = Inventory\env('DB_DATABASE');
        $this->user = Inventory\env('DB_USER');
        $this->pass = Inventory\env('DB_PASSWORD');

        // Make DSN
        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
        // Connect!
        $this->connection = new PDO($dsn, $this->user, $this->pass, $this->options);
    }

    public static function connect(): static
    {
        if (!self::$instance) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * Runs a basic query w/ params
     */
    public function query(string $sql, array $params = [], int $fetchMode = PDO::FETCH_OBJ): array
    {
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        return $stmt->fetchAll($fetchMode);
    }

    /**
     * Runs a query and returns the last insert id
     */
    public function insert(string $sql, array $params = []): int
    {
        $this->query($sql, $params);
        return (int) $this->connection->lastInsertId();
    }

    /**
     * Runs a query and returns the number of rows affected
     */
    public function update(string $sql, array $params = []): int
    {
        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        return $stmt->rowCount();
    }
}
