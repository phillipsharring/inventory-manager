<?php

declare(strict_types=1);

namespace Inventory\Db;

use Inventory;

/**
 * A simple model parent w/ table function
 */
class Model
{
    protected string $table = '';

    protected function table(string $table): string
    {
        return Inventory\env('DB_PREFIX') . $table;
    }
}
