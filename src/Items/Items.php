<?php

declare(strict_types=1);

namespace Inventory\Items;

use Inventory\Db\Connection;
use Inventory\Db\Model;
use PDO;

class Items extends Model
{
    public function __construct()
    {
        $this->table = $this->table('items');
    }

    /**
     * Get all items, with their type name mapped from the Type enum
     */
    public function all(): array
    {
        $db = Connection::connect();
        $rows = $db->query(
            sprintf('SELECT * FROM %s', $this->table),
            [],
            PDO::FETCH_ASSOC
        );
        return array_map(static function ($row) {
            return (object) array_merge($row, ['type' => Type::nameFrom($row['item_type'])]);
        }, $rows);
    }

    /**
     * Get all items, with their type name mapped from the Type enum, keyed by ID
     */
    public function byId(): array
    {
        $db = Connection::connect();
        $rows = $db->query(
            sprintf('SELECT * FROM %s', $this->table),
            [],
            PDO::FETCH_ASSOC
        );
        return array_reduce($rows, static function ($acc, $row) {
            $acc[$row['id']] = array_merge(
                $row,
                [
                    'type' => Type::nameFrom($row['item_type'])
                ]);
            return $acc;
        }, []);
    }

    /**
     * Get all item type ids, keyed by item id
     */
    public function typesById()
    {
        $db = Connection::connect();
        $rows = $db->query(sprintf('SELECT id, item_type FROM %s', $this->table));
        return array_reduce($rows, static function ($acc, $row) {
            $acc[$row->id] = $row->item_type;
            return $acc;
        }, []);
    }

    /**
     * Get all item names, keyed by item id
     */
    public function itemsById()
    {
        $db = Connection::connect();
        $rows = $db->query(sprintf('SELECT id, item FROM %s', $this->table));
        return array_reduce($rows, static function ($acc, $row) {
            $acc[$row->id] = $row->item;
            return $acc;
        }, []);
    }
}
