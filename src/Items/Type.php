<?php

declare(strict_types=1);

namespace Inventory\Items;

enum Type
{
    case OFFICE_SUPPLY;
    case EQUIPMENT;
    case FURNITURE;

    /**
     * Get the type from the item_type_id
     */
    public static function from($itemType): Type
    {
        return match ($itemType) {
            1 => Type::OFFICE_SUPPLY,
            2 => Type::EQUIPMENT,
            3 => Type::FURNITURE
        };
    }

    /**
     * Get the type name from the type
     */
    public static function name($type): string
    {
        return match ($type) {
            Type::OFFICE_SUPPLY => 'Office Supply',
            Type::EQUIPMENT     => 'Equipment',
            Type::FURNITURE     => 'Furniture',
        };
    }

    /**
     * Get the type name from the item_type_id
     */
    public static function nameFrom($itemType): string
    {
        return Type::name(Type::from($itemType));
    }
}
