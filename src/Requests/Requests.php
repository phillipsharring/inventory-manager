<?php

declare(strict_types=1);

namespace Inventory\Requests;

use Inventory\Db\Connection;
use Inventory\Db\Model;
use Inventory\Items\Items;
use Inventory\Items\Type;
use PDO;

class Requests extends Model
{
    public function __construct()
    {
        $this->table = $this->table('requests');
    }

    /**
     * Get all requests with their request type mapped by the first item in the request,
     *     the item names mapped, and the user's name capitalized
     */
    public function all(): array
    {
        // Get the item types and names by id
        $items = new Items();
        $itemTypes = $items->typesById();
        $itemNames = $items->itemsById();

        $db = Connection::connect();
        $rows = $db->query(
            sprintf('SELECT * FROM %s', $this->table),
            [],
            PDO::FETCH_ASSOC
        );
        return array_map(static function ($row) use ($itemTypes, $itemNames) {
            // Get the first item in the request, to obtain the type
            $itemIds = explode(',', $row['items']);
            $itemId = $itemIds[0];
            return (object) array_merge(
                $row,
                [
                    // Capitalize the requested by name
                    'requested_by' => ucfirst($row['requested_by']),
                    // Get the type from the first item type id
                    'request_type' => Type::nameFrom($itemTypes[$itemId]),
                    // Map the item ids to the name
                    'items' => implode(', ', array_map(static function ($itemId) use ($itemNames) {
                        return $itemNames[trim($itemId)];
                    }, explode(',', $row['items'])))
                ]
            );
        }, $rows);
    }

    /**
     * Get a single request with its item ids, types, and names mapped, and the user's name capitalized
     */
    public function get($reqId): array
    {
        // Get the item types and names by id
        $items = new Items();
        $itemsById = $items->byId();

        $db = Connection::connect();
        $rows = $db->query(
            sprintf('SELECT * FROM %s WHERE req_id = ?', $this->table),
            [$reqId],
            PDO::FETCH_ASSOC
        );
        $request = $rows[0];
        return array_merge(
            $request,
            [
                // Capitalize the requested by name
                'requested_by' => ucfirst($request['requested_by']),
                // Map the items to be almost full objects
                // A query to return item objects keyed by id could probably have been used instead
                'items' => array_map(static function ($itemId) use ($itemsById) {
                    return [
                        'id' => $itemId,
                        'item_type' => $itemsById[$itemId]['item_type'],
                        'item' => $itemsById[$itemId]['item']
                    ];
                }, explode(',', $request['items']))
            ]
        );
    }

    /**
     * Insert a request
     */
    public function add($data): int
    {
        return Connection::connect()->insert(
            sprintf('INSERT INTO %s (requested_by, items) VALUES (?, ?)', $this->table),
            [
                // Force the requested by name to lowercase
                strtolower($data['requested_by']),
                // Put all the lovely item ids into a list :(
                $this->getCleanItemIds($data['items'])
            ]
        );
    }

    /**
     * Update a request. Note the username cannot be changed. This is also enforced in the UI.
     */
    public function edit($data): int
    {
        return Connection::connect()->update(
            sprintf(
                'UPDATE %s
                SET items = ?
                WHERE req_id = ?',
                $this->table
            ),
            [
                $this->getCleanItemIds($data['items']),
                $data['req_id']
            ]
        );
    }

    /**
     * Make a summary row for a given user
     */
    public function makeSummary($user): void
    {
        // Get the item types by id
        $items = new Items();
        $itemTypeIds = $items->typesById();

        $db = Connection::connect();

        // Get all the requests for a given user
        $rows = $db->query(sprintf('SELECT * FROM %s WHERE requested_by = ?', $this->table), [$user]);
        $summaryRows = [];

        // Turn the rows into summary rows
        foreach ($rows as $row) {
            $items = explode(',', $row->items);
            $itemId = $items[0];
            $itemTypeId = $itemTypeIds[$itemId];

            // Append the items in a given request to the summary row for a given request type
            $summaryRows[$itemTypeId] = array_merge($summaryRows[$itemTypeId] ?? [], $items);
        }

        // Turn the summary rows into summary items, a formatted string
        $summaryItems = [];
        foreach($summaryRows as $itemTypeId => $items) {
            $summaryItems[] = sprintf('{%d,[%s]}', $itemTypeId, implode(',', $items));
        }

        // Put the summary rows into the formatted summary string
        $summary = sprintf('[%s]', implode(', ', $summaryItems));

        // Use the MySQL replace into to either insert or update the row.
        //    The unique constraint on the `requested_by` column makes this possible
        $db->query(
            sprintf('REPLACE INTO %s (requested_by, items) VALUES (?, ?)', $this->table('summary')),
            [
                strtolower($user),
                $summary,
            ]
        );
    }

    private function getCleanItemIds($itemIds)
    {
        return implode(',', array_filter($itemIds, function ($item) {
            return $item !== 'NaN';
        }));
    }
}
